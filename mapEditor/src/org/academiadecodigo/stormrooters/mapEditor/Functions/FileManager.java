package org.academiadecodigo.stormrooters.mapEditor.Functions;

import java.io.*;

public class FileManager {

    private FileReader reader;
    private BufferedReader buffReader;
    private FileWriter writer;
    private BufferedWriter buffWriter;

    public String readFile()   {
        String text = "";
        try {
            reader = new FileReader("/Users/codecadet/Desktop/AC_Stuff/mapEditor/src/org/academiadecodigo/stormrooters/mapEditor/Functions/sourceText.txt");
            buffReader = new BufferedReader(reader);
            text = buffReader.readLine();

            buffReader.close();

        } catch (IOException x) {

            System.out.println("Error accessing file");
        }
        return text;
    }

    public void writeFile(String txt)  {
        try {
            writer = new FileWriter("/Users/codecadet/Desktop/AC_Stuff/mapEditor/src/org/academiadecodigo/stormrooters/mapEditor/Functions/sourceText.txt");
            buffWriter = new BufferedWriter(writer);

            buffWriter.write(txt);

            buffWriter.close();
        } catch (IOException ex) {

            ex.printStackTrace();
        }

    }
}
