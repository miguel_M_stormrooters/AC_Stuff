package org.academiadecodigo.stormrooters.mapEditor.Graphic;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class Cell implements KeyboardHandler {

    private int col = 0;
    private int row = 0;
    private Rectangle rectangle;
    private Grid grid;



    public Cell(Grid grid) {

        controls();
        this.grid = grid;
        rectangle = new Rectangle(grid.getPadding(), grid.getPadding(), grid.getCellSize(), grid.getCellSize());

        rectangle.setColor(Color.MAGENTA);
        rectangle.fill();

    }

    public void controls() {
        Keyboard k = new Keyboard(this);

        int[] keys = {KeyboardEvent.KEY_W, KeyboardEvent.KEY_A, KeyboardEvent.KEY_S, KeyboardEvent.KEY_D,
                KeyboardEvent.KEY_DOWN, KeyboardEvent.KEY_LEFT, KeyboardEvent.KEY_RIGHT, KeyboardEvent.KEY_UP,
                KeyboardEvent.KEY_SPACE, KeyboardEvent.KEY_C, KeyboardEvent.KEY_L};

        for (int key : keys) {
            KeyboardEvent event = new KeyboardEvent();
            event.setKey(key);
            event.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
            k.addEventListener(event);
        }

    }

    public void move(KeyboardEvent key) {

                switch (key.getKey()) {
                    case KeyboardEvent.KEY_A:
                        col--;
                        break;
                    case KeyboardEvent.KEY_D:
                        col++;
                        break;
                    case KeyboardEvent.KEY_W:
                        row--;
                        break;
                    case KeyboardEvent.KEY_S:
                        row++;
                        break;
                    case KeyboardEvent.KEY_SPACE:
                        grid.paint(this);
                        break;
                    case KeyboardEvent.KEY_C:
                        grid.save();
                        break;
                    case KeyboardEvent.KEY_L:
                        grid.load();
                        break;
                }

                rectangle.delete();
                rectangle.translate(grid.columnToX(col) - rectangle.getX(), grid.rowToY(row) - rectangle.getY());
                rectangle.fill();


    }

    public Rectangle getRectangle() {
        return rectangle;
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

        move(keyboardEvent);
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {


    }
}
