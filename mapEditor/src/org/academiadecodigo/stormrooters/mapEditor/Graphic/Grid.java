package org.academiadecodigo.stormrooters.mapEditor.Graphic;

import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.stormrooters.mapEditor.Functions.FileManager;

import java.util.LinkedList;

public class Grid {

    private int padding = 10;
    private int cellSize = 20;
    private LinkedList<Rectangle> cells = new LinkedList<>();
    private FileManager fileManager = new FileManager();

    public Grid(int cols, int rows) {

        for (int col = 0; col < cols; col++) {
            for (int row = 0; row < rows; row++) {

                Rectangle square = new Rectangle(columnToX(col), rowToY(row), cellSize, cellSize);
                cells.add(square);
                square.draw();

            }
        }
    }

    public void paint(Cell cell) {

        for (Rectangle square : cells) {

            if (cell.getRectangle().getX() == square.getX() && cell.getRectangle().getY() == square.getY()) {

                if (square.isFilled()) {
                    square.delete();
                    square.draw();
                    continue;
                }
                square.fill();
            }


        }

    }

    public void save() {

        String text = "";

        for (Rectangle square : cells) {
            if (square.isFilled()) {

                text += 1;

                continue;
            }
            text += 0;
        }

        fileManager.writeFile(text);

    }

    public void load() {

        String txt = fileManager.readFile();

        int index = 0;

        for (Rectangle square : cells) {
            if (txt.charAt(index) == '1') {
                square.fill();
                index++;
                continue;
            }

            index++;
            square.delete();
            square.draw();

        }

    }


    public int getPadding() {
        return padding;
    }

    public int getCellSize() {
        return cellSize;
    }

    public int rowToY(int row) {
        return row * cellSize + padding;
    }

    public int columnToX(int col) {
        return col * cellSize + padding;
    }
}
