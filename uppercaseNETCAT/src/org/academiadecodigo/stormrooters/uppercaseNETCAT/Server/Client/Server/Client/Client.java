package org.academiadecodigo.stormrooters.uppercaseNETCAT.Server.Client.Server.Client;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

public class Client {

    private byte[] sendBuffer = new byte[1024];
    private byte[] rcvBuffer = new byte[1024];



    public void connect(){
           try {
            String hostName = args[0];

            int portNumber = 6666;

            byte[] sendBuffer = new byte[1024];
            byte[] recvBuffer = new byte[1024];

            DatagramSocket socket = new DatagramSocket(portNumber);

            DatagramPacket receivePacket = new DatagramPacket(recvBuffer, recvBuffer.length);
            socket.receive(receivePacket);

        } catch (IOException e) {

            System.out.println(e.getStackTrace());

        }
    }
}
