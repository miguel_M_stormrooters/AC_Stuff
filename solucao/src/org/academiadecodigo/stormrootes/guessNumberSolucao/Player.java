package org.academiadecodigo.stormrootes.guessNumberSolucao;

import org.academiadecodigo.stormrootes.guessNumberSolucao.util.RandomGenerator;

public class Player {

    private String name;

    public Player(String name) {

        this.name = name;
    }


    public int pickANumber(int maxValue) {

        return RandomGenerator.getRandom(1, maxValue);
    }

    public String getName() {

        return name;
    }
}
