package org.academiadecodigo.stormrootes.guessNumberSolucao;

import org.academiadecodigo.stormrootes.guessNumberSolucao.util.RandomGenerator;

public class Game {

    private Player[] players;

    private int maxLimit;

    public void start() {


        //Game logic and loop
        //Fr: game picks a number and saves it
        //Sec: player picks a number
        //Th: compare values
        //Fth: if they are the same the player wins if not
        //ask next player

        int systemGuess = RandomGenerator.getRandom(1, maxLimit);

        System.out.println("System guess: " + systemGuess);

        boolean endGame = false;

        while (!endGame) {
            for (Player player : players) {

                int playerGuess = player.pickANumber(maxLimit);

                System.out.println("Player " + player.getName() + " guesses: " + playerGuess);

                if (playerGuess == systemGuess) {

                    System.out.println("Winner is " + player.getName());

                    endGame = true;
                    break;
                }
            }

        }
    }

    public void setPlayers(Player[] players) {

        this.players = players;

    }

    public void setMaxLimit(int maxLimit) {

        this.maxLimit = maxLimit;

    }
}
