package org.academiadecodigo.bootcamp.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Client {

    private int port;
    private String host;

    private Socket socket;
    
    private PrintWriter toServer;
    private BufferedReader fromServer;


    public Client(String host, int port) {
        this.port = port;
        this.host = host;
    }

    public void start() {

        try {

            socket = new Socket(host, port);
            toServer = new PrintWriter(socket.getOutputStream(), true);
            fromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            while (!socket.isClosed()) {

                sendMessage();
                receiveMessage();
            }

        } catch (UnknownHostException e) {
            System.err.println("Invalid host or port: " + host + ":" + port);

        } catch (IOException e) {
            System.err.println(e.getMessage());

        } finally {

            close();
        }
    }

    private void sendMessage() {

        String messageToSend = getInput();

        toServer.println(messageToSend);

        if (messageToSend.equals("/exit")) {
            close();
        }
    }

    private void receiveMessage() throws IOException {

        if (socket.isClosed()) {
            return;
        }

        String receivedMessage = fromServer.readLine();

        System.out.println(receivedMessage);
    }

    private String getInput() {

        Scanner scanner = new Scanner(System.in);
        System.out.print("You: ");

        return scanner.nextLine();
    }

    private void close() {

        try {

            if (socket != null && !socket.isClosed()) {
                socket.close();
            }

        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
