package org.academiadecodigo.bootcamp.client;

public class Main {


    public static void main(String[] args) {

        int port = 8080;

        Client client = new Client("localhost", port);

        client.start();
    }
}
