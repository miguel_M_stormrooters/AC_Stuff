package org.academiadecodigo.bootcamp.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server {

    private final int port;


    public Server(int port) {
        this.port = port;
    }

    public void start() {

        try {
            ServerSocket serverSocket = new ServerSocket(port);

            while (!serverSocket.isClosed()) {
                Socket clientConnection = serverSocket.accept();

                talkToClient(clientConnection);


            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void talkToClient(Socket clientConnection) {

        try {

            BufferedReader fromClient = new BufferedReader(new InputStreamReader(clientConnection.getInputStream()));
            PrintWriter toClient = new PrintWriter(clientConnection.getOutputStream(), true);

            while (true) {

                String messageFromClient = fromClient.readLine();

                if (messageFromClient == null) {
                    System.out.println("Client hung up on me...");
                    break;
                }

                if (messageFromClient.equals("/exit")) {
                    return;
                }

                toClient.println(getInput());
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {

            try {

                System.out.println("Closing everything...");
                clientConnection.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String getInput() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Message to send to Client: ");

        return scanner.nextLine();
    }
}
