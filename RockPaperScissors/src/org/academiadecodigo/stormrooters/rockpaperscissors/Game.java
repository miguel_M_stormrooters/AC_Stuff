package org.academiadecodigo.stormrooters.rockpaperscissors;

public class Game {

    private int maxRounds;

    private Player player1;

    private Player player2;

    private int currentRound = 1;


    public void setPlayer1(Player player1) {
        this.player1 = player1;
    }

    public void setPlayer2(Player player2) {
        this.player2 = player2;
    }

    public void start() {

        playHands();


        while (player1.getHandsWon() == player2.getHandsWon()) {

            maxRounds++;
            playHands();


        }

        if (player1.getHandsWon() > player2.getHandsWon()) {

            System.out.println("Victory : Player 1");

        } else {

            System.out.println("Victory : Player 2");
        }
    }

    public void playHands() {

        System.out.println("Total rounds: " + maxRounds);

        while (currentRound <= maxRounds) {

            Hand newPlayerHand = player1.pickAHand();

            System.out.println("Player one chose: " + newPlayerHand);

            Hand newPlayer2Hand = player2.pickAHand();

            System.out.println("Player two chose: " + newPlayer2Hand);


            if (newPlayerHand == newPlayer2Hand) {

                System.out.println("It's a tie");

            } else if (newPlayerHand == Hand.ROCK && newPlayer2Hand == Hand.SCISSORS ||
                    newPlayerHand == Hand.PAPER && newPlayer2Hand == Hand.ROCK ||
                    newPlayerHand == Hand.SCISSORS && newPlayer2Hand == Hand.PAPER) {

                System.out.println("Player 1 wins this round");
                player1.wins();

            } else {

                System.out.println("Player 1 lost this round; victory for player 2");
                player2.wins();
            }


            System.out.println("Player one score: " + player1.getHandsWon() + " Player two score: " + player2.getHandsWon());
            currentRound++;
        }

    }

    public void setMaxRounds(int maxRounds) {

        this.maxRounds = maxRounds;

    }


}




