package org.academiadecodigo.stormrooters.rockpaperscissors;

public class Main {

    public static void main(String[] args) {


        Game newGame = new Game();

        Player newPlayer = new Player();

        newPlayer.setName("Herculano");

        Player newPlayer2 = new Player();

        newPlayer2.setName("Ermelindo");

        newGame.setMaxRounds(3);

        newGame.setPlayer1(newPlayer);

        newGame.setPlayer2(newPlayer2);


        newGame.start();


    }

}
