package org.academiadecodigo.stormrooters.rockpaperscissors;


public class Player {

    private String name;

    private int handsWon;


    public void setName(String name) {

        this.name = name;

    }

    public String getName(){

        return name;
    }

    public Hand pickAHand(){

        Hand[] hands = Hand.values();
        int index = Random.randomNumber(0, 2);

       return hands[index];


    }

    public int getHandsWon(){

        return handsWon;
    }

    public void wins(){

        this.handsWon++;
    }

}
