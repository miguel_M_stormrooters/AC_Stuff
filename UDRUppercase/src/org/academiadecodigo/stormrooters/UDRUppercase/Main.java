package org.academiadecodigo.stormrooters.UDRUppercase;

import org.academiadecodigo.stormrooters.UDRUppercase.Server.Server;

public class Main {

    public static void main(String[] args) {

        Server server = new Server("localhost");
        server.receive();
        server.capitalize();
    }
}
