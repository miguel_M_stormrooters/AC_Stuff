package org.academiadecodigo.stormrooters.UDRUppercase.Client;

import org.academiadecodigo.stormrooters.UDRUppercase.Server.Server;

import java.io.IOException;
import java.net.*;

public class Client {


    private int portNumber;
    private byte[] sendBuffer;
    private byte[] recvBuffer = new byte[1024];


    public void transmit(String message){


        try {

            DatagramSocket clientSocket = new DatagramSocket();

            sendBuffer = message.getBytes();

            DatagramPacket sendPacket = new DatagramPacket(sendBuffer, sendBuffer.length, InetAddress.getLocalHost(), Server.portNumber);
            clientSocket.send(sendPacket);

            System.out.println("Package sent: " + new String(sendBuffer));

            DatagramPacket receivePacket = new DatagramPacket(recvBuffer, recvBuffer.length);
            clientSocket.receive(receivePacket);

            System.out.println("Message received: " + new String(recvBuffer, 0, message.length()));

            clientSocket.close();


        } catch (SocketException e) {

            System.out.println(e.getStackTrace());

        } catch (IOException e) {
            e.printStackTrace();
        }



    }




}
