package org.academiadecodigo.stormrooters.UDRUppercase.Server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

public class Server {

    private String hostName;
    private byte[] sendBuffer;
    private byte[] rcvBuffer = new byte[1024];
    public static final int portNumber = 6666;
    private DatagramSocket serverSocket;
    private DatagramPacket receivedPacket;

    public Server(String hostName) {

        this.hostName = hostName;

        try {
            serverSocket = new DatagramSocket(portNumber);
        } catch (SocketException e) {
            e.printStackTrace();
        }

    }

    public void receive() {
        try {

            receivedPacket = new DatagramPacket(rcvBuffer, rcvBuffer.length);

            serverSocket.receive(receivedPacket);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void capitalize() {

     String S = new String(rcvBuffer);
     String sUpper = S.toUpperCase();
     sendBuffer = sUpper.getBytes();

        DatagramPacket sendPacket = new DatagramPacket(sendBuffer, sendBuffer.length, receivedPacket.getAddress(), receivedPacket.getPort());

        try {
            serverSocket.send(sendPacket);

        } catch (IOException e) {

            e.printStackTrace();
        }

        serverSocket.close();

    }


}
