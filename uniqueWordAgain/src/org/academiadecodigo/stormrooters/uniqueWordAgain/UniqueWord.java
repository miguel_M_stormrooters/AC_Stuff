package org.academiadecodigo.stormrooters.uniqueWordAgain;


import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;

public class UniqueWord implements Iterable<String> {

    private HashSet<String> stringHashSet = new HashSet<>();


    public void add(String string) {

        String[] wordSet = string.split(" ");

        System.out.println("The set of words is as follows: " + Arrays.toString(wordSet));
        for (String word : wordSet) {

            stringHashSet.add(word);

        }

        System.out.println("individual words: " + stringHashSet.toString());

    }


    @Override
    public Iterator<String> iterator() {
        return null;
    }
}
