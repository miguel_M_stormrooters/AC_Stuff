package org.academiadecodigo.stormrooters.MoneyInTheBank;

public class Main {

    public static void main(String[] args) {

        String name = "Reinaldo";

        Person person = new Person();
        person.setName(name);


        Bank bank = new Bank();
        bank.addMoney(10);
        person.setMyAccount(bank);


        Wallet wallet = new Wallet();
        wallet.setMoney(0);
        person.setMyWallet(wallet);


        System.out.println(person.getName() + ", you currently have " + wallet.getMoney() + " € in your wallet");

        //System.out.println("In your bank account you have " + bank.addMoney());
        person.goShopping(70);




    }

}
