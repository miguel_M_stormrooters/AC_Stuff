package org.academiadecodigo.stormrooters.MoneyInTheBank;

public class Wallet {

    private int money;

    public void addMoney(int amount) {

        this.money += amount;

    }

    public int takeMoney(int amount) {

        this.money -= amount;
        return amount;
    }

    public int getMoney(){

        return this.money;

    }

    public void setMoney(int money) {
        this.money = money;
    }


}
