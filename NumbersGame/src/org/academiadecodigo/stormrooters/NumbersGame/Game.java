package org.academiadecodigo.stormrooters.NumbersGame;


public class Game {

    private int maxLimit;
    private Player[] players;

    public Game(int limit) {

        this.maxLimit = limit;
    }


    public void start() {

        System.out.println("My max limit: " + maxLimit); // my max limit is unnecessary

        int numberPicked = Generator.numberPicked(maxLimit);


        boolean gameOn = true;

        while (gameOn) {


            for (int i = 0; i < players.length; i++) {

                int playerNumber = players[i].pickANumber(maxLimit);

                System.out.println(players[i].getName() + " results were: " + playerNumber + " Computer: " + numberPicked);

                if (playerNumber == numberPicked) {

                    System.out.println("Great success! " + players[i].getName() + " wins the game!");

                    gameOn = false;
                    break;
                }
            }
        }


    }

    public void setMaxLimit(int maxLimit) {

        this.maxLimit = maxLimit;
    }

    public void setPlayer(Player[] players) {

        this.players = players;

    }

}




