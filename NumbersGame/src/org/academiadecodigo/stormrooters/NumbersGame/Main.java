package org.academiadecodigo.stormrooters.NumbersGame;

public class Main {

    public static void main(String[] args) {

        Game newGame = new Game(10);

        Player gamer = new Player();

        gamer.setName("Arsenio");

        Player gamer1 = new Player();

        gamer1.setName("Suzette");

        Player gamer2 = new Player();

        gamer2.setName("Ermelindo");

        Player[] gamers = {gamer, gamer1, gamer2};

        gamer.setTroll(true);


        System.out.println("Is it true this is a troll? " + gamer.getTroll());


        newGame.setPlayer(gamers);
        newGame.setMaxLimit(20);


        newGame.start();
    }
}
