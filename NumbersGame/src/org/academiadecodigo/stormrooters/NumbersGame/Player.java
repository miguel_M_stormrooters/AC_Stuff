package org.academiadecodigo.stormrooters.NumbersGame;

public class Player {

    private String name;
    private boolean troll;
    //create getter and setter
    //on main set the value to true
    //then print the value on main

    public void setTroll(boolean troll) {


        this.troll = troll;

    }

    public boolean getTroll() {

        return this.troll;

    }

    public void setName(String name) {

        this.name = name;
    }

    public String getName() {

        return name;
    }

    public int pickANumber(int limit) {

        return Generator.numberPicked(limit);
    }


}

