package org.academiadecodigo.stormrooters.mapEditor2;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class Cell implements KeyboardHandler {

    private int cols;
    private int rows;
    private Grid grid;
    private Rectangle rectangle;


    public Cell(Grid grid) {

        controls();
        this.grid = grid;
        rectangle = new Rectangle(grid.getPadding(), grid.getPadding(), grid.getCellSize(), grid.getCellSize());
        rectangle.setColor(Color.BLACK);
        rectangle.fill();

    }

    public void controls() {


        Keyboard keyboard = new Keyboard(this);

        int[] keys = {KeyboardEvent.KEY_UP, KeyboardEvent.KEY_DOWN,
                KeyboardEvent.KEY_RIGHT, KeyboardEvent.KEY_LEFT,
                KeyboardEvent.KEY_SPACE, KeyboardEvent.KEY_C, KeyboardEvent.KEY_L};

        for (int key : keys) {

            KeyboardEvent event = new KeyboardEvent();
            event.setKey(key);
            event.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
            keyboard.addEventListener(event);

        }

    }

    public void move(KeyboardEvent key) {

        switch (key.getKey()) {
            case KeyboardEvent.KEY_LEFT:
                if (cols == 0) {
                    cols = 0;
                    break;
                }
                cols--;
                break;

            case KeyboardEvent.KEY_RIGHT:
                if(cols == grid.getCols() -1){

                    cols = grid.getCols() -1 ;
                    break;
                }

                cols++;
                break;

            case KeyboardEvent.KEY_UP:
                if(rows == 0){

                    rows = 0;
                    break;
                }


                rows--;
                break;

            case KeyboardEvent.KEY_DOWN:
                if(rows == grid.getRows() -1){

                    rows = grid.getRows() -1;
                    break;

                }
                rows++;
                break;


        }


        rectangle.translate(grid.columnToX(cols) - rectangle.getX(), grid.rowToY(rows) - rectangle.getY());

    }


    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        System.out.println(keyboardEvent.getKey());
        move(keyboardEvent);
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }

}
