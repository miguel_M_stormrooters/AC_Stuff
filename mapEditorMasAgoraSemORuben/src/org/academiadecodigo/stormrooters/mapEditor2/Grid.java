package org.academiadecodigo.stormrooters.mapEditor2;

import org.academiadecodigo.simplegraphics.graphics.Rectangle;

import java.util.LinkedList;

public class Grid {

    private int padding = 10;
    private int cellSize = 20;
    private LinkedList<Rectangle> cells = new LinkedList<>();
    private int cols;
    private int rows;

    public Grid(int cols, int rows) {

        this.cols = cols;
        this.rows = rows;

        for (int col = 0; col < cols; col++) {
            for (int row = 0; row < rows; row++) {

                Rectangle square = new Rectangle(columnToX(col), rowToY(row), cellSize, cellSize);
                cells.add(square);
                square.draw();
            }

        }

    }

    public int getCols() {
        return cols;
    }

    public int getRows() {
        return rows;
    }

    public int getCellSize() {
        return cellSize;
    }

    public int getPadding() {
        return padding;
    }

    public int columnToX(int cols) {
        return cols * cellSize + padding;

    }

    public int rowToY(int rows) {

        return rows * cellSize + padding;

    }
}
