package org.academiadecodigo.stormrooters.Histogram;

import java.util.HashMap;
import java.util.Iterator;

public class HistogramComposition implements Iterable<String> {


    private HashMap<String, Integer> histogramMap = new HashMap<>();


    public void put(String string) {


        String[] wordNumber = string.split(" ");

        for (String word : wordNumber) {

            if(histogramMap.containsKey(word)){

                histogramMap.put(word, histogramMap.get(word) + 1);
                continue;
            }

            histogramMap.put(word, 1);


        }
        System.out.println(histogramMap.entrySet());
    }

    @Override
    public Iterator<String> iterator() {
        return histogramMap.keySet().iterator();
    }
}
