package org.academiadecodigo.stormrooters.Histogram;

import java.util.HashMap;

public class HistogramInheritance extends HashMap<String, Integer> {

    public HistogramInheritance(String string){


        String[] wordNumber = string.split(" ");

        for (String word : wordNumber) {

            if(containsKey(word)){

                put(word, get(word) + 1);
                continue;
            }

            put(word, 1);


        }
        System.out.println(entrySet());

    }

}
