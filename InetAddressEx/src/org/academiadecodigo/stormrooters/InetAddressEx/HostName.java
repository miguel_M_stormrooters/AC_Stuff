package org.academiadecodigo.stormrooters.InetAddressEx;


import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;

public class HostName {

    public static void main(String[] args) throws UnknownHostException {

        String hostName = getUserInput();
        int timeout = 1000;

        try {

            InetAddress address = InetAddress.getByName(hostName);
            System.out.println(hostName + " has the address: " + address.getHostName() +
                    (address.isReachable(timeout) ? " can " : " can't ") + "be reached");

        } catch (IOException ex2) {
            System.err.println(hostName + "doot");
        }

    }

    private static String getUserInput() {

        Scanner scanner = new Scanner(System.in);
        String question = "what is the site you're trying to access? ";

        System.out.println(question);
        return scanner.nextLine();

    }
}
