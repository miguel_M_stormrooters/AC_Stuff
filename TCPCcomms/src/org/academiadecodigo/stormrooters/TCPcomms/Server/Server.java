package org.academiadecodigo.stormrooters.TCPcomms.Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private int portNumber = 6969;
    ServerSocket serverSocket;


    public void receive() {

        try {

            serverSocket = new ServerSocket(portNumber);
            System.out.println("Waiting for connection...");

            Socket clientSocket = serverSocket.accept();
            System.out.println("Connection received!");

            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            out.println("Welcome to my chat");

            while (!serverSocket.isClosed()) {

                String message;

                if ((message = in.readLine()) != null) {

                    if (message.toLowerCase().equals("exitchat")) {
                        out.println("connection terminated. ");
                        clientSocket.close();
                        break;

                    }

                    System.out.println(message);
                    out.println(message.toUpperCase() + " sent.");
                }

            }


        } catch (IOException e) {

            e.printStackTrace();
        }

    }
}
