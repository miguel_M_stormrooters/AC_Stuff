package org.academiadecodigo.stormrooters.TCPcomms.Client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    private int portNumber = 6969;
    private String hostName = "localhost";


    public void transmit(String message) {

        try {

            Socket clientSocket = new Socket(hostName, portNumber);
            System.out.println("Current connection: " + hostName + " at " + portNumber);

            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            System.out.println(in.readLine());

            Scanner scanner = new Scanner(System.in);

            while (!clientSocket.isClosed()) {

                String massage;

                if ((massage = scanner.nextLine()).toLowerCase().equals("exitchat")){

                    System.out.println("<<<< CONNECTION TERMINATED >>>>");

                    clientSocket.close();

                    break;
                }
                out.println(massage);
                System.out.println(in.readLine());
            }


        } catch (IOException e) {

            e.printStackTrace();
        }

    }

}