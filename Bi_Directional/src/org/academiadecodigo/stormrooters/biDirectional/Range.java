package org.academiadecodigo.stormrooters.biDirectional;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Range implements Iterable<Integer> {

    private int min;
    private int max;
    private boolean forward;

    public Range(int min, int max) {
        this.min = min;
        this.max = max;
    }

    public void iterateForward() {
        this.forward = true;
    }

    public void iterateBackwards(){
        this.forward = false;

    }

    @Override
    public Iterator<Integer> iterator() {

        if (forward) {
            return new Iterator<Integer>() {
                private int currentPosition = min - 1;

                @Override
                public boolean hasNext() {
                    return currentPosition < max;
                }

                @Override
                public Integer next() {

                    if (!hasNext()) {

                        throw new NoSuchElementException("No more elements");
                    }

                    currentPosition++;
                    return currentPosition;
                }
            };
        }

        return new Iterator<Integer>() {
            private int currentPosition = max + 1;

            @Override
            public boolean hasNext() {
                return currentPosition > min;
            }


            @Override
            public Integer next() {

                if (!hasNext()) {

                    throw new NoSuchElementException("No more elements");
                }

                currentPosition--;
                return currentPosition;
            }
        };

    }
}



