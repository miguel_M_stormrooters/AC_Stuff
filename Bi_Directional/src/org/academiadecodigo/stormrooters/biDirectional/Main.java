package org.academiadecodigo.stormrooters.biDirectional;

public class Main {

    public static void main(String[] args) {

        Range range = new Range(1, 4);


        range.iterateBackwards();

        for (Integer i : range){

            System.out.println(i);

        }

        System.out.println("=================");

        range.iterateForward();

        for (Integer i : range){

            System.out.println(i);

        }
    }
}
