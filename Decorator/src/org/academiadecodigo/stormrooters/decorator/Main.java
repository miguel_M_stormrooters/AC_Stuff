package org.academiadecodigo.stormrooters.decorator;

public class Main {

    public static void main(String[] args) {

        Room room = new BearRug(new ItalianLeatherCouch(new BasicLivingRoom()));

        System.out.println("Room decor: " + room.getAccessories());

        System.out.println("Total cost: " + room.getPrice());

    }
}
