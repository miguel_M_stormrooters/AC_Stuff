package org.academiadecodigo.stormrooters.decorator;

public interface Room {

    String getAccessories();
    int getPrice();
}
