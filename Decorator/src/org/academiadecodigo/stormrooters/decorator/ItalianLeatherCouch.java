package org.academiadecodigo.stormrooters.decorator;

public class ItalianLeatherCouch extends RoomDecorator{


    public ItalianLeatherCouch(Room decoratedRoom) {
        super(decoratedRoom);
    }

    @Override
    public String getAccessories() {
        return super.getAccessories() + " with Italian Leather couch";
    }

    @Override
    public int getPrice() {
        return super.getPrice() + 1200;
    }
}
