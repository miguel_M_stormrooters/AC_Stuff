package org.academiadecodigo.stormrooters.decorator;

public class BasicLivingRoom implements Room{


    @Override
    public String getAccessories() {
        return "Basic living room";
    }

    @Override
    public int getPrice() {
        return 800;
    }
}
