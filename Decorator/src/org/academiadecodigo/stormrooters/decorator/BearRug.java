package org.academiadecodigo.stormrooters.decorator;

public class BearRug extends RoomDecorator {

    public BearRug(Room decoratedRoom) {
        super(decoratedRoom);
    }

    @Override
    public String getAccessories() {
        return super.getAccessories() + " and includes an elegant Bear Rug";
    }

    @Override
    public int getPrice() {
        return super.getPrice() + 2000;
    }
}
