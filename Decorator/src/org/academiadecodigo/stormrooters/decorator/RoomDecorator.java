package org.academiadecodigo.stormrooters.decorator;

public class RoomDecorator implements Room {


    private Room decoratedRoom;

    public RoomDecorator(Room decoratedRoom){

        this.decoratedRoom = decoratedRoom;
    }

    @Override
    public String getAccessories() {
        return decoratedRoom.getAccessories();
    }

    @Override
    public int getPrice() {
        return decoratedRoom.getPrice();
    }
}
