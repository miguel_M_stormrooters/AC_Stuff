package org.academiadecodigo.stormrooters.theGamesAfoot;

public class Hound {

    private String name;

    public Hound(String name) {

        this.name = name;
    }


    public void scare(Rabbit bugsBunny){


            if(!bugsBunny.isAlive()) {
                System.out.println("Rabbit is dead already. ");
                return;
            }

            if(bugsBunny.isScared()){
                System.out.println("It's already scared. ");
                return;

        }

        bugsBunny.beScared();
        System.out.println("woof woof");
    }
}
