package org.academiadecodigo.stormrooters.theGamesAfoot;

public class Hunter {

    private String name;
    private Shotgun shotgun;
    private Hound hound;

    public Hunter(String name) {

        this.name = name;
    }

    public void setHound(Hound hound) {
        this.hound = hound;

    }

    public void setShotgun(Shotgun shotgun) {

        this.shotgun = shotgun;
    }

    public void hunt(Rabbit bugsBunny) {

        hound.scare(bugsBunny);

        shotgun.shoot(bugsBunny);

    }


}