package org.academiadecodigo.stormrooters.theGamesAfoot;

public class Rabbit {

    private boolean alive;
    private boolean scared;


    public void beScared() {

        this.scared = true;
    }


    public void beDead() {

        this.scared = false;
    }


    public boolean isAlive() {

        return this.alive;
    }

    public boolean isScared() {

        return this.scared;
    }
}
