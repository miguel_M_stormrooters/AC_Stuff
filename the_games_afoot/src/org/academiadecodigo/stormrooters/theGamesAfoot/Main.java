package org.academiadecodigo.stormrooters.theGamesAfoot;

public class Main {

    public static void main(String[] args) {

        Hound hound = new Hound("Pluto");

        Hunter hunter = new Hunter("Quévin");

        Shotgun shotgun = new Shotgun();

        Rabbit rabbit = new Rabbit();

        shotgun.setNumberOfShells(2);
        hunter.setHound(hound);
        hunter.setShotgun(shotgun);

        hunter.hunt(rabbit);
    }
}
