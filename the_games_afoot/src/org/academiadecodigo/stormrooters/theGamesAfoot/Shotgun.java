package org.academiadecodigo.stormrooters.theGamesAfoot;

public class Shotgun {

    private int numberOfShells;

    public void setNumberOfShells(int numberOfShells) {

        this.numberOfShells = numberOfShells;

    }

    public void shoot(Rabbit rabbit) {

        if (!rabbit.isAlive()) {
            System.out.println("Dead rabbit ");
            return;
        }
        if (!rabbit.isScared()) {
            System.out.println("Not scared ");
            return;
        }

        if (numberOfShells <= 0) {
            System.out.println("*no bullets*");

            return;

        }

        numberOfShells--;
        rabbit.beDead();
        System.out.println("Killed a rabbit ");
    }

}
