package org.academiadecodigo.stormrooters.stringManip;

public class stringManip {

    public static void main(String[] args) {
        String str = "http://www.academiadecodigo.org";

        String domain = "";  //www.academiadecodigo.org

        String name = "";   // < Academia de Codigo_ >

        String[] words = str.split("//");

        domain = words[1];

        name = domain.substring(5, 12);

        String capitalA = "A";

        String capitalC = "C";

        String iAm = "I am a Code Cadet at ";

        String de = domain.substring(12, 14);

        String codigo = domain.substring(15, 20);

        System.out.println(iAm + "<" + capitalA.concat(name) + " " + de + " " + capitalC + codigo + "_ >" + " " + str);


    }

}
