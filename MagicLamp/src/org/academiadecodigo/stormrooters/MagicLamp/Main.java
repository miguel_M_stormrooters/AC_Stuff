package org.academiadecodigo.stormrooters.MagicLamp;

public class Main {


    public static void main(String[] args) {


        Lamp lamp1 = new Lamp();

        try {

            while(lamp1.getRub() < lamp1.getMaxRubs()-1){

                Genie genie = lamp1.rub();

                for(int i = 1; i < 6; i++){

                    genie.grantWish();

                }


            }

            Genie demon = lamp1.rub();
            lamp1.recharge(demon);
            lamp1.recharge(demon);


        } catch (DemonReleasedException ex){

        }
    }
}
