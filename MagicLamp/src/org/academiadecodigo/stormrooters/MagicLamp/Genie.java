package org.academiadecodigo.stormrooters.MagicLamp;

public class Genie {

    private int maxWishes;
    private int grantedWish;

    public Genie() {

        this.maxWishes = 5;

    }

    public void grantWish() {

        if (grantedWish < maxWishes) {
            System.out.println("Wish granted! ");

            grantedWish++;

        } else
            System.out.println("You have no more wishes to beseech ");
    }

    public int getMaxWishes() {

        return maxWishes;
    }


    public int grantedWish() {

        return grantedWish;


    }



}

