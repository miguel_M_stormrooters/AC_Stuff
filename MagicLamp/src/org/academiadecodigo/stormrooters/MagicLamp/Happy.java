package org.academiadecodigo.stormrooters.MagicLamp;

public class Happy extends Genie {


    @Override
    public void grantWish() {

        if (grantedWish() >= getMaxWishes()) {

            System.out.println("All your dreams have come true. Hooray. ");
            return;
        }

        System.out.println("You still have " + (getMaxWishes() - grantedWish()) + " wishes to request");
        super.grantWish();
    }

}

