package org.academiadecodigo.stormrooters.MagicLamp;

public class Lamp {

    private int rub = 0;
    private int maxRubs = 10;



    public int getRub() {
        return rub;
    }


    public Genie rub() throws DemonReleasedException {

        rub++;

        if (rub > maxRubs) {

            System.out.println("You rubbed it as much as you possibly could ( ͡° ͜ʖ ͡°) ");
            throw new DemonReleasedException();

        } else if (rub == maxRubs) {
            Genie recycledDemon = new Demon();

            System.out.println(" (!)  a demon appeared! ");
            return recycledDemon;


        } else if (rub % 2 == 0) {

            Genie happyGenie = new Happy();

            System.out.println("  (!)  a happy genie appeared! ");
            return happyGenie;

        } else {

            Genie grumpyGenie = new Grumpy();
            System.out.println("  (!)  a grumpy genie appeared! ");
            return grumpyGenie;
        }
    }

    public int getMaxRubs() {
        return maxRubs;
    }

    public void recharge(Genie demon) {

        if (demon instanceof Demon) {

            rub = 0;
            ((Demon) demon).recycle();

            System.out.println("Recharge!!");
        }
    }
}
