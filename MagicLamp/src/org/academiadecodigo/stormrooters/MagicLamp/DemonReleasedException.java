package org.academiadecodigo.stormrooters.MagicLamp;

public class DemonReleasedException extends Exception {

    public DemonReleasedException() {


        System.out.println("No more genies available. ");

    }

    public DemonReleasedException(String message) {

        super(message);
    }
}
