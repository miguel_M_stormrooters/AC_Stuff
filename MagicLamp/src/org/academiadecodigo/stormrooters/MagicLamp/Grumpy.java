package org.academiadecodigo.stormrooters.MagicLamp;

public class Grumpy extends Genie {

    private boolean grantedWish = false;

    @Override
    public void grantWish() {

        if (!grantedWish) {

            grantedWish = true;

            System.out.println("Your one wish has been granted ");
            return;
        }

        System.out.println("Your wish has been granted. I grant but one wish, mortal ");

    }
}
