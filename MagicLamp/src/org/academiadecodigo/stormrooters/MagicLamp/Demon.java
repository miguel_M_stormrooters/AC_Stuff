package org.academiadecodigo.stormrooters.MagicLamp;

public class Demon extends Genie {


    private boolean recycled = false;


    @Override
    public void grantWish() {


        if (!recycled) {

            System.out.println("I grant you all the wishes you desire");
            return;
        }

        System.out.println("I cannot grant further wishes. ");


    }

    public void recycle() {

        if (recycled) {

            System.out.println("I can only be recycled once. ");
        }

        recycled = true;
    }
}
