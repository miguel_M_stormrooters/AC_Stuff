package org.academiadecodigo.stormrooters.simpleWebServer;

import javax.activation.MimetypesFileTypeMap;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {


    private static final int PORT_NUMBER = 6969;

    private ServerSocket serverSocket;
    private Socket clientSocket;
    private File file;
    private MimetypesFileTypeMap map;

    public Server(){
        this.map = new MimetypesFileTypeMap();
        map.addMimeTypes("text/css css");
        map.addMimeTypes("image/png png");
    }


    public void start() {
        try {
            serverSocket = new ServerSocket(PORT_NUMBER);

            while (!serverSocket.isClosed()) {
                receive();
                write();
                close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void receive() {


        try {

            System.out.println("Awaiting connection... ");

            clientSocket = serverSocket.accept();
            System.out.println("Connected! ");

            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            String[] message = in.readLine().split(" ");

            if (message[0].equals("GET")) {

                System.out.println(Arrays.toString(message));
                searchFile(message[1]);

            }


        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void searchFile(String fileName) {

        file = new File("files" + fileName);


        if (file.exists()) {

            System.out.println("File searched: " + fileName + " file found: " + file);

        } else {

            System.out.println("File not found");
        }


    }

    public void write() {


        try {

            BufferedInputStream fromFile = new BufferedInputStream(new FileInputStream(file));

            byte[] buffer = new byte[1024];
            int countOfReadBytes = 0;

            DataOutputStream outputStream = new DataOutputStream(clientSocket.getOutputStream());
            String mimeType = map.getContentType(file.getName());

            String header = "HTTP/1.1 200 Document Follows\r\n" +
                    "Content-Type: " + mimeType + "; charset=UTF-8\r\n" +
                    "Content-Length: " + file.length() + "\r\n\r\n";

            outputStream.writeBytes(header);

            while (countOfReadBytes != -1) {
                outputStream.write(buffer, 0, countOfReadBytes);
                countOfReadBytes = fromFile.read(buffer);
            }

            outputStream.flush();

        } catch (IOException e) {
            System.err.println("File not Found");

        }
    }



    public void close() {

        try {

            clientSocket.close();

        } catch (IOException e)

        {
            e.printStackTrace();
        }
    }
}


