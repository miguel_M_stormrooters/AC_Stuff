package org.academiadecodigo.stormrooters.HotelSimulation;

public class Room {

    private boolean availability = true;


    public void setAvailability(boolean availability) {

        this.availability = availability;
    }

    public boolean getAvailability() {

        return this.availability;

    }

}

