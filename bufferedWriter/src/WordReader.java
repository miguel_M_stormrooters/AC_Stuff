import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

public class WordReader implements Iterable<String> {

    private String fileName;

    public WordReader(String fileName) {
        this.fileName = fileName;
    }


    @Override
    public Iterator<String> iterator() {

        return new Iterator<String>() {

            private int currentPos = -1;
            private String line;
            private String[] result;
            private FileReader reader;
            private BufferedReader buffReader;

            @Override
            public boolean hasNext() {
                try {

                    reader = new FileReader(fileName);
                    buffReader = new BufferedReader(reader);

                    if (result != null && currentPos < result.length -1) {
                        return true;
                    }

                    if ((line = buffReader.readLine()) != null) {

                        result = line.split(" ");
                        currentPos = -1;
                    }


                } catch (IOException ex) {

                }

                return currentPos < result.length -1;

            }

            @Override
            public String next() {

                if (!hasNext()) {
                    return "No more file to read. ";
                }

                currentPos++;
                return result[currentPos];
            }

        };

    }
}
