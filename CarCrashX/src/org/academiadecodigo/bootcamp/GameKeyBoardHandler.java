package org.academiadecodigo.bootcamp;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

import java.awt.event.KeyEvent;

public class GameKeyBoardHandler implements KeyboardHandler {

    public void controls() {

        Keyboard k = new Keyboard(this);
        KeyboardEvent keyLeft = new KeyboardEvent();

        keyLeft.setKey(KeyboardEvent.KEY_LEFT);
        keyLeft.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        k.addEventListener(keyLeft);

        KeyboardEvent keyRight = new KeyboardEvent();

        keyRight.setKey(KeyboardEvent.KEY_RIGHT);
        keyRight.setKeyboardEventType((KeyboardEventType.KEY_PRESSED));
        k.addEventListener(keyRight);

        KeyboardEvent keyUp = new KeyboardEvent();

        keyUp.setKey(KeyboardEvent.KEY_UP);
        keyUp.setKeyboardEventType((KeyboardEventType.KEY_PRESSED));
        k.addEventListener(keyUp);

        KeyboardEvent keyDown = new KeyboardEvent();

        keyDown.setKey((KeyboardEvent.KEY_DOWN));
        keyDown.setKeyboardEventType((KeyboardEventType.KEY_PRESSED));
        k.addEventListener((keyDown));


    Keyboard k2 = new Keyboard(this);
        KeyboardEvent keyA = new KeyboardEvent();

        keyA.setKey(KeyboardEvent.KEY_A);
        keyA.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        k.addEventListener(keyA);

        KeyboardEvent keyD = new KeyboardEvent();

        keyD.setKey(KeyboardEvent.KEY_D);
        keyD.setKeyboardEventType((KeyboardEventType.KEY_PRESSED));
        k.addEventListener(keyD);

        KeyboardEvent keyW = new KeyboardEvent();

        keyW.setKey(KeyboardEvent.KEY_W);
        keyW.setKeyboardEventType((KeyboardEventType.KEY_PRESSED));
        k.addEventListener(keyW);

        KeyboardEvent keyS = new KeyboardEvent();

        keyS.setKey((KeyboardEvent.KEY_S));
        keyS.setKeyboardEventType((KeyboardEventType.KEY_PRESSED));
        k.addEventListener((keyS));

    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }
}
