package org.academiadecodigo.bootcamp;

import org.academiadecodigo.bootcamp.car.Car;
import org.academiadecodigo.bootcamp.car.CarFactory;
import org.academiadecodigo.bootcamp.grid.*;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;

/**
 * The game logic
 */
public class Game extends GameKeyBoardHandler {


    public Car controllableCar;

    public Car controllableCarLuigi;

    /**
     * Graphical Car field
     */
    private Grid grid;

    /**
     * Container of Cars
     */
    private Car[] cars;

    /**
     * Animation delay
     */
    private int delay;

    /**
     * The collision detector
     */
    private CollisionDetector collisionDetector;

    /**
     * Number of cars to manufacture
     */
    private int manufacturedCars = 999 ;


    /**
     * Constructs a new game
     *
     * @param gridType which grid type to use
     * @param cols     number of columns in the grid
     * @param rows     number of rows in the grid
     * @param delay    animation delay
     */
    Game(GridType gridType, int cols, int rows, int delay) {

        grid = GridFactory.makeGrid(gridType, cols, rows);
        this.delay = delay;

    }

    /**
     * Creates a bunch of cars and randomly puts them in the field
     */
    public void init() {

        grid.init();

        cars = new Car[manufacturedCars];
        collisionDetector = new CollisionDetector(cars);


        for (int i = 0; i < manufacturedCars; i++) {

            cars[i] = CarFactory.getNewCar(grid);
            cars[i].setCollisionDetector(collisionDetector);
            cars[i].setGrid(grid);

        }


        controllableCar = CarFactory.getNewCar(grid);
        controllableCar.setCollisionDetector(collisionDetector);
        controllableCar.setGrid(grid);
        controllableCar.getPos().setColor(GridColor.NOCOLOR);
        collisionDetector.check(controllableCar);

        controllableCarLuigi = CarFactory.getNewCar(grid);
        controllableCarLuigi.setCollisionDetector(collisionDetector);
        controllableCarLuigi.setGrid(grid);
        controllableCarLuigi.getPos().setColor(GridColor.MAGENTA);
        controls();

        collisionDetector.check(controllableCarLuigi);
    }


    /**
     * Starts the animation
     *
     * @throws InterruptedException
     */
    public void start() throws InterruptedException {

        while (true) {

            // Pause for a while
            Thread.sleep(delay);

            moveAllCars();

        }

    }

    /**
     * Moves all cars
     */
    public void moveAllCars() {

        for (Car c : cars) {
            c.move();
            collisionDetector.check(c);
            collisionDetector.check(controllableCar);
            collisionDetector.check(controllableCarLuigi);
        }

    }

    public void moveCar(KeyboardEvent keyboardEvent) {

        if (controllableCar.isCrashed()) {
            return;
        }

        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_UP:
                controllableCar.getPos().moveInDirection(GridDirection.UP, 1);
                break;
            case KeyboardEvent.KEY_DOWN:
                controllableCar.getPos().moveInDirection(GridDirection.DOWN, 1);
                break;
            case KeyboardEvent.KEY_LEFT:
                controllableCar.getPos().moveInDirection(GridDirection.LEFT, 1);
                break;
            case KeyboardEvent.KEY_RIGHT:
                controllableCar.getPos().moveInDirection(GridDirection.RIGHT, 1);
                break;
        }


    }

    public void moveCarLuigi(KeyboardEvent keyboardEvent) {

        if (controllableCarLuigi.isCrashed()) {
            return;
        }

        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_W:
                controllableCarLuigi.getPos().moveInDirection(GridDirection.UP, 1);
                break;
            case KeyboardEvent.KEY_S:
                controllableCarLuigi.getPos().moveInDirection(GridDirection.DOWN, 1);
                break;
            case KeyboardEvent.KEY_A:
                controllableCarLuigi.getPos().moveInDirection(GridDirection.LEFT, 1);
                break;
            case KeyboardEvent.KEY_D:
                controllableCarLuigi.getPos().moveInDirection(GridDirection.RIGHT, 1);
                break;
        }


    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

        if (controllableCar.getPos().equals(controllableCarLuigi.getPos())) {
            controllableCar.crash();
            controllableCarLuigi.crash();
        }

        moveCar(keyboardEvent);

        moveCarLuigi(keyboardEvent);
    }
}
