package org.academiadecodigo.bootcamp.gfx.simplegfx;

import org.academiadecodigo.bootcamp.grid.GridColor;
import org.academiadecodigo.bootcamp.grid.GridDirection;
import org.academiadecodigo.bootcamp.grid.position.AbstractGridPosition;
import org.academiadecodigo.bootcamp.grid.position.GridPosition;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

/**
 * Simple graphics position
 */
public class SimpleGfxGridPosition extends AbstractGridPosition {

    private Rectangle rectangle;
    private SimpleGfxGrid simpleGfxGrid;


    /**
     * Simple graphics position constructor
     *
     * @param grid Simple graphics grid
     */
    public SimpleGfxGridPosition(SimpleGfxGrid grid) {
        super((int) (Math.random() * grid.getCols()), (int) (Math.random() * grid.getRows()), grid);
        this.simpleGfxGrid = grid;
        this.rectangle = new Rectangle(simpleGfxGrid.columnToX(super.getCol()), simpleGfxGrid.rowToY(super.getRow()),
                simpleGfxGrid.getCellSize(), simpleGfxGrid.getCellSize());
        rectangle.draw();
    }

    /**
     * Simple graphics position constructor
     *
     * @param col  position column
     * @param row  position row
     * @param grid Simple graphics grid
     */
    public SimpleGfxGridPosition(int col, int row, SimpleGfxGrid grid) {
        super(col, row, grid);
        this.simpleGfxGrid = grid;
        this.rectangle = new Rectangle(SimpleGfxGrid.PADDING + col * grid.getCellSize(),

                SimpleGfxGrid.PADDING + row * grid.getCellSize(), grid.getCellSize(), grid.getCellSize());
        rectangle.draw();

    }

    /**
     * @see GridPosition#show()
     */
    @Override
    public void show() {

        rectangle.fill();

    }

    /**
     * @see GridPosition#hide()
     */
    @Override
    public void hide() {

        rectangle.delete();

    }

    /**
     * @see GridPosition#moveInDirection(GridDirection, int)
     */
    @Override
    public void moveInDirection(GridDirection direction, int distance) {

        super.setPos(getCol(), getRow());
        super.moveInDirection(direction, distance);

        rectangle.translate(simpleGfxGrid.columnToX(getCol()) - rectangle.getX(), simpleGfxGrid.rowToY(getRow()) - rectangle.getY());


    }

    /**
     * @see AbstractGridPosition#setColor(GridColor)
     */
    @Override
    public void setColor(GridColor color) {

        super.setColor(color);
        switch (color) {
            case BLUE:
                rectangle.setColor(Color.BLUE);
                break;
            case RED:
                rectangle.setColor(Color.RED);
                break;
            case GREEN:
                rectangle.setColor(Color.GREEN);
                break;
            case MAGENTA:
                rectangle.setColor(Color.MAGENTA);
                break;
            default:
                rectangle.setColor(Color.BLACK);
                break;
        }
    }
}
