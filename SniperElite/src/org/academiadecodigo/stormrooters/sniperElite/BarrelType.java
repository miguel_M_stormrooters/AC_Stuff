package org.academiadecodigo.stormrooters.sniperElite;

public enum BarrelType {


    PLASTIC(30),
    WOOD(40),
    METAL(50);

    private int maxDamage;

    BarrelType(int maxDamage) {

        this.maxDamage = maxDamage;
    }

    public int getMaxDamage() {

        return maxDamage;
    }
}
