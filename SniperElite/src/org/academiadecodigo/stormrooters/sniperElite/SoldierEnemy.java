package org.academiadecodigo.stormrooters.sniperElite;

public class SoldierEnemy extends Enemy {

    public SoldierEnemy() {

        super(100);
    }

    @Override
    public String getMessage() {

        return "It's a soldier! Starts with 100 health";
    }

    @Override
    public int getHealth() {
        return super.getHealth();
    }
}
