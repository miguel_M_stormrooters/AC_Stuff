package org.academiadecodigo.stormrooters.sniperElite;

public interface Destroyable {

    public abstract void hit(int damage);

    public abstract boolean isDestroyed();


}
