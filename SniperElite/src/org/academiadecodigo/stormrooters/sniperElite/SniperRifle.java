package org.academiadecodigo.stormrooters.sniperElite;

public class SniperRifle {

    private int bulletDamage = 30;


    public void shoot(Destroyable enemy) {


        int hitProb = Random.randomNumber(1, 10);

        if (hitProb < 10) {

            enemy.hit(bulletDamage);

            return;
        }

        System.out.println("You missed the enemy. ");



    }

}
