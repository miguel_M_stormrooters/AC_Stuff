package org.academiadecodigo.stormrooters.sniperElite;

public class Barrel extends GameObject implements Destroyable {


    BarrelType barrelType;
    public int currentDamage;
    public boolean destroyed;


    public Barrel() {
        barrelType();
    }

    public void barrelType() {

        int pickBarrelType = Random.randomNumber(0, 2);

        switch (pickBarrelType) {

            case (0):
                barrelType = BarrelType.PLASTIC;
                break;

            case (1):
                barrelType = BarrelType.WOOD;
                break;

            case (2):
                barrelType = BarrelType.METAL;
                break;

        }

    }

    @Override
    public void hit(int damage) {




        currentDamage += damage;

        System.out.println("You hit the barrel and take " + currentDamage + " out of the maximum barrel HP: " + barrelType.getMaxDamage());

        if (currentDamage >= barrelType.getMaxDamage()){

            destroyed = true;
            System.out.println("You destroyed the barrel. ");
        }

    }



    @Override
    public boolean isDestroyed() {

        return destroyed;
    }

    @Override
    public String getMessage() {
        return "You spot a " + barrelType + " barrel";
    }
}
