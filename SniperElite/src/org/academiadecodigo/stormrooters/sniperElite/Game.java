package org.academiadecodigo.stormrooters.sniperElite;


public class Game {

    private GameObject[] gameObject;
    private SniperRifle sniperRifle = new SniperRifle();
    private int shotsFired;


    public void createObjects() {

        this.gameObject = new GameObject[3];

        for (int i = 0; i < gameObject.length; i++) {

            int randomNumber = (int) Math.ceil(Math.random() * 100);

            if(randomNumber <= 25){

                gameObject[i] = new Barrel();
                continue;
            }

            if (randomNumber <= 50) {

                gameObject[i] = new SoldierEnemy();

                continue;
            }


            if (randomNumber <= 75) {

                gameObject[i] = new ArmouredEnemy();

                continue;
            }

            gameObject[i] = new Tree();

        }


    }


    public void start() {

        for (int i = 0; i < gameObject.length; i++) {

            System.out.println("\n" + gameObject[i].getMessage());



            if (gameObject[i] instanceof Destroyable) {

                Destroyable target = (Destroyable) gameObject[i];

                while (!target.isDestroyed()) {

                    System.out.println(" > > > You took your shot ");
                    sniperRifle.shoot(target);
                    shotsFired++;


                }


            }
        }


        System.out.println("You fired: " + shotsFired + " shots.");
    }


}
