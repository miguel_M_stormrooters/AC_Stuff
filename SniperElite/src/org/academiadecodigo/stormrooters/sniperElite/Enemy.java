package org.academiadecodigo.stormrooters.sniperElite;

public abstract class Enemy extends GameObject implements Destroyable{

    private int health;
    private boolean isDestroyed = false;


    public Enemy(int health) {

        this.health = health;

    }


    public void hit(int damage) {



        health = health - damage;

        System.out.println((health <= 0) ? "Your final shot killed the enemy" : "You hit him and his health is now: " + health);

        if (health <= 0) {

            isDestroyed = true;
            return;
        }



    }

    public int getHealth() {

        return health;
    }

    public boolean isDestroyed() {

        return isDestroyed;
    }

}
