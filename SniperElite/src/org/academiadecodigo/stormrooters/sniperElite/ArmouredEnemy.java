package org.academiadecodigo.stormrooters.sniperElite;

public class ArmouredEnemy extends Enemy {

    private int armour;

    public ArmouredEnemy() {

        super(100);

        this.armour = 50;

    }

    @Override
    public void hit(int damage) {


        if (armour > 0) {
            System.out.println("The armor took: " + damage + " damage.");
            this.armour -= damage;
            return;
        }

        super.hit(damage);

    }

    @Override
    public String getMessage() {

        return "It's an armoured soldier! It has a 50+ armour bonus ";
    }

    @Override
    public int getHealth() {
        return super.getHealth() + armour;
    }
}
