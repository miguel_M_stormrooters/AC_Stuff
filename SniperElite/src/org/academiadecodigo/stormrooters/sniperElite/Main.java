package org.academiadecodigo.stormrooters.sniperElite;

public class Main {

    public static void main(String[] args) {

        Game game = new Game();
        game.createObjects();
        game.start();
    }

}
