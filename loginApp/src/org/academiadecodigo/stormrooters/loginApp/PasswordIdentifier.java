package org.academiadecodigo.stormrooters.loginApp;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.string.StringInputScanner;
import org.academiadecodigo.bootcamp.scanners.string.StringSetInputScanner;

import java.util.HashMap;
import java.util.Map;


public class PasswordIdentifier {

    public static void main(String[] args) {

        Prompt prompt = new Prompt(System.in, System.out);

        Map<String, String> verification = new HashMap<>();
        verification.put("Passos Dias", "Aguiar Mota");
        verification.put("Miguel", "supersafepassword123");
        verification.put("Adolfo Dias", "III Reich");


        StringInputScanner username = new StringSetInputScanner(verification.keySet());
        username.setMessage("What is your username? ");
        username.setError(" Username not found..");


        StringInputScanner password = new StringInputScanner();
        password.setMessage("Password: ");

        String name = prompt.getUserInput(username);
        String pass = prompt.getUserInput(password);


        while (!verification.get(name).equals(pass)) {

            System.out.println("Wrong password. Please try again: ");

            pass = prompt.getUserInput(password);

        }

        System.out.println("Login successful. Welcome, " + name + "!");

    }
}
