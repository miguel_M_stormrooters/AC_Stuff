package org.academiadecodigo.stormrooters.toDoList;

public enum Importance {

    HIGH,
    MEDIUM,
    LOW

}
