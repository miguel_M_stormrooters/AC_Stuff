package org.academiadecodigo.stormrooters.toDoList;

public class ToDo implements Comparable<ToDo> {

    private Importance importanceLevel;

    private int priority;

    private String item;


    public ToDo(Importance importanceLevel, int priority, String item) {

        this.importanceLevel = importanceLevel;
        this.priority = priority;
        this.item = item;

    }


    @Override
    public int compareTo(ToDo o) {

        switch (o.importanceLevel) {
            case LOW:

                if (importanceLevel == Importance.LOW) {
                    return this.priority - o.priority;
                }
                return -1;

            case MEDIUM:

                if (importanceLevel == Importance.MEDIUM) {
                    return this.priority - o.priority;

                }


                if (importanceLevel == Importance.HIGH) {

                    return -1;
                }

                return 1;

            case HIGH:

                if (importanceLevel == Importance.HIGH) {
                    return this.priority - o.priority;


                }
                return 1;

        }

        return 0;

    }


    public int getPriority() {
        return priority;
    }

    @Override
    public String toString() {
        return importanceLevel.toString() + " " + priority + " " + item;
    }
}
