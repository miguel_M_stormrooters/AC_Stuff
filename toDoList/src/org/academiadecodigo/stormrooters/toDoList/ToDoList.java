package org.academiadecodigo.stormrooters.toDoList;

import java.util.PriorityQueue;

public class ToDoList {

    private PriorityQueue<ToDo> queue = new PriorityQueue<ToDo>();

    public void add(Importance importance, int priority, String item) {


        ToDo toDoList = new ToDo(importance, priority, item);

        queue.add(toDoList);
        System.out.println("A new To-Do List was made. ");

    }

    public ToDo remove() {
        System.out.println(queue.peek());
        return queue.remove();


    }

    public boolean isEmpty() {

        return queue.isEmpty();
    }

}
