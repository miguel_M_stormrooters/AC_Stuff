package org.academiadecodigo.stormrooters.copyStuff;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Copy {

    byte[] buffer;

    private FileInputStream inputStream;
    private FileOutputStream outputStream;

    public Copy() throws FileNotFoundException {

    }

    public void readFile() throws IOException {

        try {

            inputStream = new FileInputStream("/Users/codecadet/Desktop/mysteryFile.jpg");
            outputStream = new FileOutputStream("/Users/codecadet/Downloads/mysteryFile.jpg");

            buffer = new byte[6];                                       //
                                                                        //reads file, saves value in buffer
            int originalFile = inputStream.read(buffer);                //

            while (originalFile != -1) {

                outputStream.write(buffer, 0, originalFile);
                originalFile = inputStream.read(buffer);                //updates the value (number) of bytes read
                System.out.println("Bytes read: " + originalFile);

            }

        } catch (IOException x) {

            System.out.println("Error accessing your files. ");         //in case something goes awry, catches exception

        } finally {

            closeStreams();
        }

    }

    public void closeStreams() {

        try {
            inputStream.close();

        } catch (IOException ex) {
            System.out.println("there was cagalhão in your code");   //tries to close the input stream first, since
                                                                    //output stream is dependant on it to close

        }
        try {
            outputStream.close();

        } catch (IOException ex2) {

            System.out.println("there was cagalhão in your code");
        }


    }
}






















