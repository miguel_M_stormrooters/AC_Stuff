package org.academiadecodigo.stormrooters.fileException.FileClasses;

import org.academiadecodigo.stormrooters.fileException.FileClasses.exceptions.FileNotFoundException;
import org.academiadecodigo.stormrooters.fileException.FileClasses.exceptions.NotEnoughPermissionsException;
import org.academiadecodigo.stormrooters.fileException.FileClasses.exceptions.NotEnoughSpaceException;

public class FileManager {

    private boolean login = false;
    private File[] files;
    private int numberOfFiles = 0;


    public FileManager(int totalFiles) {

        files = new File[totalFiles];


    }

    public void login() {

        if (login) {

            System.out.println("Already logged in. ");
            return;
        }

        this.login = true;
        System.out.println("__logged in");

    }

    public void logout() {


        if (!login) {

            System.out.println("Can't logout if you're not logged in, duh. ");
            return;
        }

        this.login = false;
        System.out.println("__logged out");

    }

    public File getFile(String name) throws FileNotFoundException {

        File file = null;


        for (int i = 0; i < numberOfFiles; i++) {


            if (files[i].getName().equals(name)) {

                System.out.println("You found the file you were looking for: " + name);
                file = files[i];
                return file;
            }


        }

        throw new FileNotFoundException("File not found. ");

    }


    public void createFile(String name) throws NotEnoughPermissionsException, NotEnoughSpaceException {

        if (!login) {

            throw new NotEnoughPermissionsException("You're not logged in. ");

        }

        if (numberOfFiles >= files.length) {

            throw new NotEnoughSpaceException("You don't have available space. ");
        }

        if (numberOfFiles < files.length) {


            files[numberOfFiles] = new File(name);

            numberOfFiles++;
            System.out.println("Created new file with the name " + name);
        }


    }

}
