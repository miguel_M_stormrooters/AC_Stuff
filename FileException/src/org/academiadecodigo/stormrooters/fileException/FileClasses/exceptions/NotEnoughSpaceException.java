package org.academiadecodigo.stormrooters.fileException.FileClasses.exceptions;

public class NotEnoughSpaceException extends FileException {

    public NotEnoughSpaceException(){
    }

    public NotEnoughSpaceException(String s){
        super(s);
    }

}
