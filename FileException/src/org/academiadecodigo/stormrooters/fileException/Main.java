package org.academiadecodigo.stormrooters.fileException;

import org.academiadecodigo.stormrooters.fileException.FileClasses.FileManager;
import org.academiadecodigo.stormrooters.fileException.FileClasses.exceptions.FileNotFoundException;
import org.academiadecodigo.stormrooters.fileException.FileClasses.exceptions.NotEnoughPermissionsException;
import org.academiadecodigo.stormrooters.fileException.FileClasses.exceptions.NotEnoughSpaceException;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {


        FileManager fileManager = new FileManager(10);

        fileManager.login();

        try {

            fileManager.createFile("Donald Trumps' Art of the Deal");
            fileManager.createFile("How to Build a Wall for Dummies");
            fileManager.createFile("Russian Phonebook");
            fileManager.createFile("Hot Latinas Weekly");



        } catch (NotEnoughPermissionsException e) {
            System.out.println(e.getMessage());
        } catch (NotEnoughSpaceException e) {
            System.out.println(e.getMessage());
        }

        try {
            fileManager.getFile("Hot Latinas Weekly");
        } catch (FileNotFoundException n){
            System.out.println(n.getMessage());
        }

    }
}
