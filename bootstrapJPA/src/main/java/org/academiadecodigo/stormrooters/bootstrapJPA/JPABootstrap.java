package org.academiadecodigo.stormrooters.bootstrapJPA;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.RollbackException;

public class JPABootstrap {

    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("test");


    public Customer addOrUpdate(Customer customer) {

        EntityManager entityManager = emf.createEntityManager();

        try {

            entityManager.getTransaction().begin();

            Customer savedCustomer = entityManager.merge(customer);

            entityManager.getTransaction().commit();

            return savedCustomer;

        } catch (RollbackException e) {

            entityManager.getTransaction().rollback();

        } finally {

            if (entityManager != null) {

                entityManager.close();
            }

        }
        return null;
    }

    public  Customer findByID(Integer id) {

        EntityManager entityManager = emf.createEntityManager();

        try {
            return entityManager.find(Customer.class, id);

        } finally {

            if (entityManager != null) {
                entityManager.close();
            }
        }

    }
}
