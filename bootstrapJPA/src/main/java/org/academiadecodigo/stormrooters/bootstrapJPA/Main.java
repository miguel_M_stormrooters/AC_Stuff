package org.academiadecodigo.stormrooters.bootstrapJPA;



public class Main {

    public static void main(String[] args) {

        Customer customer = new Customer();
        Customer customer2 = new Customer();

        JPABootstrap bootstrapBill = new JPABootstrap();

        //initial values set to customers
        customer.setName("António Silva");
        customer2.setName("André Batata");

        customer.setSexualOrientation("Apache Helicopter");
        customer2.setSexualOrientation("Pansexual");

        //customer below updates value on table so that the version keeps track of the changes made
        customer = bootstrapBill.addOrUpdate(customer);
        bootstrapBill.addOrUpdate(customer2);

        //update to the initial value
        customer.setName("Batata");

        //same as above
        customer = bootstrapBill.addOrUpdate(customer);

        //always necessary to update object to the state in which it is in the database
        customer.setSexualOrientation("Bicurious");
        bootstrapBill.addOrUpdate(customer);


    }

}
