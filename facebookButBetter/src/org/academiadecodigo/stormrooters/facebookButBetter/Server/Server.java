package org.academiadecodigo.stormrooters.facebookButBetter.Server;


import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {

    private final int port;
    LinkedList<Worker> workers = new LinkedList<>();

    public Server(int port) {

        this.port = port;
    }


    public void start() {


        ExecutorService cachedPool = Executors.newCachedThreadPool();

        try {

            ServerSocket serverSocket = new ServerSocket(port);


            while (!serverSocket.isClosed()) {

                Socket clientConnection = serverSocket.accept();

                System.out.println("Connection established!");

                Worker worker = new Worker(clientConnection);
                workers.add(worker);
                cachedPool.submit(worker);


            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {

            cachedPool.shutdown();

        }

    }

    private void broadcast(String message, Worker slave) {


        for (Worker worker : workers) {


            if (worker != slave) {
                worker.toClient.println(slave.name + " : " + message);

            }


        }


    }

    private void whisper(String message, String receiver, String sender) {


        for (Worker worker : workers) {

            if (worker.name.equals(receiver)) {

                worker.toClient.println(sender + " : " + message);
            }

        }


    }

    private class Worker implements Runnable {

        private String name;

        private Socket clientConnection;
        private PrintWriter toClient;

        private Worker(Socket clientConnection) {

            this.clientConnection = clientConnection;

        }


        private void receiveMessage() {


            try {
                BufferedReader fromClient = new BufferedReader(new InputStreamReader(clientConnection.getInputStream()));
                toClient = new PrintWriter(clientConnection.getOutputStream(), true);


                toClient.println("What is your name, boii? ");
                name = fromClient.readLine();
                toClient.println("Welcome, " + name + " !");

                while (true) {


                    String messageFromClient = fromClient.readLine();

                    switch (messageFromClient) {

                        case "/exit":

                            return;


                        case "/whisper":

                            toClient.println("To whom do you want to whisper?");

                            String name = fromClient.readLine();

                            toClient.println("What message do you want to whisper ? ");

                            String message = fromClient.readLine();

                            whisper(message, name, this.name);

                            break;

                        default:

                            broadcast(messageFromClient, this);
                    }


                    if (messageFromClient == null) {

                        System.out.println("Your connection went down... ");
                        return;

                    }


                }

            } catch (IOException e) {
                e.printStackTrace();
            } finally {


                try {

                    System.out.println("shutting down... ");
                    clientConnection.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }


        }


        @Override
        public void run() {

            receiveMessage();

        }
    }

}
