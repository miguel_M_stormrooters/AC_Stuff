package org.academiadecodigo.stormrooters.facebookButBetter.Client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Client {


    private int port;
    private String host;
    private Socket socket;
    private PrintWriter toServer;
    private BufferedReader fromServer;
    private String name;

    public Client(String host, int port) {

        this.port = port;
        this.host = host;
       // this.name = name;

    }


    public void start() {

        boolean chatOn = true;
        getName();

        try {

            socket = new Socket(host, port);
            toServer = new PrintWriter(socket.getOutputStream(), true);
            fromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            while (chatOn) {

                sendMessage();
                receiveMessage();

                if(getInput().equals("/exit")){

                    chatOn = false;

                }

            }

        } catch (UnknownHostException e) {
            System.err.println("invalid host or port:" + host + ":" + port);
        } catch (IOException e) {
            System.err.println(e.getMessage());

        } finally {

            //close();
        }


    }

    private void receiveMessage() throws IOException {

        if (socket.isClosed()) {

            return;
        }

        String receivedMessage = fromServer.readLine();

        System.out.println(receivedMessage);


    }

    private void sendMessage() {

        String message = getInput();

        toServer.println(message);

        if (message.equals("/exit")) {

            close();



        }

    }


    private String getInput() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("YOU: ");
        return scanner.nextLine();


    }

    private String getName() {

        return name;
    }

    private void close() {

        if (socket != null && !socket.isClosed()) {


            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
